@extends('admins._layouts.form-base')

@section('page-title')
    Change Password
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        Change Password
    </li>
@endsection

@section('form-body')
{!! Form::suitOpen(array('route'=> suitRouteName('password'))) !!}
    <div class="form-body">
        {!! Form::suitPassword('current_password', 'Current Password') !!}
        {!! Form::suitPassword('new_password', 'New Password') !!}
        {!! Form::suitPassword('new_password_confirmation', 'New Password Confirmation') !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
