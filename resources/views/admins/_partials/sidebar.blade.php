@extends('admins._layouts.sidebar')

@section('sidebar-menus')
<li{!! $routePrefix == 'html-templates' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('html-templates.index') }}">
        <i class="fa fa-html5"></i>
        <span class="title">Html Template</span>
    </a>
</li>
<li{!! $routePrefix == 'menus' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('menus.index') }}">
        <i class="icon-directions"></i>
        <span class="title">Menu</span>
    </a>
</li>
<li{!! $routePrefix == 'pages' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('pages.index') }}">
        <i class="icon-grid"></i>
        <span class="title">Page</span>
    </a>
</li>
<li{!! $routePrefix == 'users' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('users.index') }}">
        <i class="icon-user"></i>
        <span class="title">User</span>
    </a>
</li>
@endsection
