<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:url" content="{{ $metaUrl or Request::url() }}">
<meta name="twitter:title" content="{{ $metaTitle or Setting::get('site-name') }}">
<meta name="twitter:description" content="{{ $metaDescription or Setting::get('site-description') }}">
<meta name="twitter:image" content="{{ $metaImage or (empty(Setting::get('site-image'))?'':asset(Setting::get('site-image'))) }}">
