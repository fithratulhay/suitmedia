[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# Installation

1. Clone this repository (**`git clone git@bitbucket.org:suitmedia/suitcms.git --depth 1`**)
1. Create dan configure `.env` file based on `.env.example`. (Read Section Configuration)
1. Run `composer install` in the root project to install all dependencies including develeopment requirement.
1. Run `php artisan key:generate` in the root project to generate new Key for new Application.
1. Run `php artisan migrate` in the root project to migrate main suitcms database.
1. Create username and password for admin `php artisan user:new-admin [your username] [your email] [your password]`
1. Try login in with url `/secret/login`
1. Done!

[Next: Configuration](https://gitlab.com/suitmedia/suitcms/wikis/Configuration)
