[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# View Structure

All view templates of suitcms are put inside **admins** directory.

## Layout

All view layouts of suitcms are put inside **_layouts** directory. There are two base layout which are used on suitcms,

1. **_layouts.default**, this is default layout
1. **_layouts.login**, this layout only used for login-like page

Other than these two there are two additional layout used for resource layout, they are

1. **_layouts.form-one-column**, used for form layout
1. **_layouts.index-hide-show**, used for form index layout or mainly for datatable

## Element

Here are elements which are include on other views.

1. **_includes.flash**, flash message
1. **_includes.header**, header menu
1. **_includes.page-header**, Page header encompasses breadcrump and page title
1. **_includes.sidebar**, Sidebar menu

## Templates

SuitCms has created templates used to used for module views. The location is on **_templates**. Here is the process to create new module views:

1. Copy the **_templates** directory and rename it.
1. For simple module, **_templates.create** and **_templates.edit** no need to be changed.
1. For form, edit `form-body` body content in **_templates.form**,
1. For index, edit`table-column-checkbox` content in **_templates.index**.

Please note, if edit and create form has differences, you can use `$model->exists` to check whether it'is create or edit form.

[Next: Site Settings](https://gitlab.com/suitmedia/suitcms/wikis/SiteSetting)
