[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# ResourceController (`App\Http\Controllers\Admin\ResourceController`)

The goal of this Class is to reduce implementation time of admin controller.

## How To Extends

Here is the example of class that extends `ResouceController`. For simple module you just need to define to dependencies injection.

```
#!php
<?php
    namespace App\Http\Controllers\Admin;

    use App\Model\Page as Model;                // Changed this to current controller model
    use App\Http\Form\Admin\PageForm as Form;   // Changed this to implementation of BaseForm for current form

    class PageController extends ResourceController
    {
        protected $viewPrefix;  // Set Custom View Prefix. default is plural and sanke case with separator '_'. e.g pages, page_attachments

        protected $routePrefix; // Set Custom Route Prefix. default is plural and snake case with separator `-`. e.g pages, page-attachment

        protected $pageName;    // Set Custom Page Name

        public function __construct(Model $model, Form $form)
        {
            parent::__construct($model, $form);
        }
    }
?>

```

## Convention

This class have convention for `Route::resource()` and view directory name. Both route and view follow plural form and snake case of the controller name. Here are some example

Controller Name | View Prefix | RoutePrefix
----------------|-------------|------------
PageController  | pages | pages
PersonController |  people | people
PageCategoryController | page_categories | page-categories

If you don't want to follow these convention you must define their prefix on the controller class

```
#!php
<?php
    class NewController extends ResourceController
    {
        protected $viewPrefix = 'custom-views'; // Set Custom View Prefix. default is plural and sanke case with separator '_'. e.g pages, page_attachments

        protected $routePrefix = 'custom-routes'; // Set Custom Route Prefix. default is plural and snake case with separator `-`. e.g pages, page-attachment
    }
?>
```

## Controller Action Method Flow

This section only list method call flow used on all action. All the method will be explain on next section.

### index

1. `paginate()`/`paginateQuery()`

### create

1. `formData()`

### store

1. `beforeValidate()`
1. `afterValidate()`
1. `doSave()`

### edit

1. `find($key)`
1. `formData()`

### update

1. `find($key)`
1. `beforeValidate()`
1. `afterValidate()`
1. `doSave()`

### destroy

1. `find($key)`
1. `delete()`

## Passing Form Data to View

Commonly create and update form are all the same. So the form data which are being pass to each view are the same. So `formData()` method are called on create and edit action. Here is example code

```
#!php
<?php
    protected function formData()
    {
        parent::formData();
        \View::share('authors', $this->authorList());
        \View::share('layouts', $this->model->layoutList());
        \View::share('categories', $this->categoryList());
        \View::share('parents', ['' => 'No Parent'] + $this->parentList());
    }
?>
```

If one day, create and update have differences required data, use `$this->model->exists` to check whether it's exists or not.

## Passing Data to BaseForm

Just like passing to view but the implementation is on `beforeValidate()` instead of formData. `beforeValidate()` method is called right before form validation.

```
#!php
<?php
    protected function beforeValidate()
    {
        parent::beforeValidate();
        $this->form->setPageId($this->model->id);
        $this->form->setAuthorList(array_keys($this->authorList()));
        $this->form->setParentList(array_keys($this->parentList()));
        $this->form->setCategoryList(array_keys($this->categoryList()));
        $this->form->setLayoutList(array_keys($this->model->layoutList()));
    }
?>
```

As you can see, `formData()` and `beforeValidate()` are very similar implementation.

## Save Request Data

The next step after the request being validated is saving the data (`doSave()`). If you have relationship data to be save, you need to overide this method. **Don't forget to call parent method** Here is the example

```
#!php
<?php

    protected function doSave()
    {
        parent::doSave();
        if (!empty($this->form->input('categories'))) {
            $this->model->categories()->sync($this->form->input('categories'));
        }
    }
?>

```

Keep it in mind not every implementation after `parent::doSave()`. It depend on relationship to be save.

* Has Many -> after  `parent::doSave()`
* Has One -> after `parent::doSave()`
* Belongs To -> before `parent::doSave()`
* Many To Many -> after `parent::doSave()`

[Next: View Structure](https://gitlab.com/suitmedia/suitcms/wikis/ViewStructure)
