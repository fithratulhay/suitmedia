[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# PublishToggleTrait (`App\Http\Controllers\Extension\PublishToggleTrait`)
New traits to enable toggle publish object in index view

## Use
To use this trait, there are several step to do:
* Add `PublishToggleTrait` to controller, example

```php
<?php

namespace App\Http\Controllers\Admin;

use App\Model\Page as Model;
use App\Http\Controllers\Extension\PublishToggleTrait;

class PageController extends ResourceController
{
    use PublishToggleTrait;

```

* Add new route in `admin_route` like this,

```php
    // Custom Route to change published in Index
    \Route::post('pages/publish/{key}', [
        'as' => suitRouteName('pages.publish'),
        'uses' => 'PageController@publish'
    ]);
```

*  Add this line to your `index-ajax.blade.php`

`{!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},`

* Don't forget to add `published` column to your `index.blade.php` like this,

```php
@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="title">Title</label>
    <label><input type="checkbox" checked data-name="parent.title">Parent</label>
    <label><input type="checkbox" checked data-name="description">Description</label>
    <label><input type="checkbox" checked data-name="published_at">Published</label> // published column
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
```

**For module that use this trait, you can see at `Page` module**

**For view that use this trait, you can see at `resources/views/admins/pages` or `resources/views/admins/_templates`**