<?php

return [
    'site-name' => [
        'title' => 'Site Name',
        'description' => 'Website Name',
        'type' => 'suitText',
        'value' => 'Suitcms'
    ],
    'site-description' => [
        'title' => 'Site Description',
        'Description' => 'Website description',
        'type' => 'suitTextarea',
        'value' => ''
    ],
    'site-image' => [
        'title' => 'Site Image',
        'Description' => 'Website image for social media',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'logo-url' => [
        'title' => 'Admin Logo Url',
        'type' => 'suitFileBrowser',
        'value' => 'assets/admin/img/logo.png'
    ],
    'social-facebook-username' => [
        'title' => 'Facebook username',
        'type' => 'suitText',
        'value' => ''
    ],
    'social-twitter-username' => [
        'title' => 'Twitter username',
        'type' => 'suitText',
        'value' => ''
    ]
];
