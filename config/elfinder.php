<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */
    'dir' => [],

    /*
    |--------------------------------------------------------------------------
    | Filesystem disks (Flysytem)
    |--------------------------------------------------------------------------
    |
    | Define an array of Filesystem disks, which use Flysystem.
    | You can set extra options, example:
    |
    | 'my-disk' => [
    |        'URL' => url('to/disk'),
    |        'alias' => 'Local storage',
    |    ]
    */
    'disks' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */

    'route' => [
        'prefix' => 'elfinder',
        'middleware' => 'admin.auth', //Set to null to disable middleware filter
    ],

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => [
        [
            'driver' => 'LocalFileSystem',
            'path'   => public_path('files'),
            'URL'    => '/files/',
            'attributes' => array(
                array(
                    'pattern' => '/\/\./',
                    'read' => false,
                    'write' => false,
                    'hidden' => true,
                    'locked' => false
                ),
            ),
            'uploadOrder' => ['allow', 'deny'],
            'uploadAllow' => [
                'image',
                'video/mov',
                'video/quicktime',
                'video/mp4',
                'application', // excel, pdf, etc
            ],
            'uploadDeny' => [
                'php',
                'phtml',
                'php3',
                'pl',
                'cgi',
                'py',
            ],

            // uploadOverwrite
            // Replace files with the same name on upload or give them new names.
            // true - replace old files,
            // false - give new names like original_name-number.ext
            'uploadOverwrite' => false,

            // copyOverwrite
            // Replace files on paste or give new names to pasted files.
            // true - old file will be replaced with new one,
            // false - new file get name - original_name-number.ext
            'copyOverwrite' => false,

            // List of commands disabled on this root
            'disabled' => ['mkfile']
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => [],

);
