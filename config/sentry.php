<?php

return array(
    'dsn' => env('GETSENTRY_DNS'),

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),
);
