<?php

namespace TestApp\Supports\Attachment\Downloader;

use App\Supports\Attachment\Downloader\UrlDownloader;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Message\Response;
use GuzzleHttp\RedirectMiddleware;
use Illuminate\Contracts\Filesystem\Filesystem;
use Mockery;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use TestApp\TestCase;

class UrlDownloaderTest extends TestCase
{
    protected $filesystem;

    protected $client;

    protected $downloader;

    public function setUp()
    {
        parent::setUp();

        $filesystem = Mockery::mock(Filesystem::class);
        $client = Mockery::mock(Client::class);
        $extensionGuesser = new MimeTypeExtensionGuesser;
        $downloader = new UrlDownloader($filesystem, $extensionGuesser, $client);

        $this->filesystem = $filesystem;
        $this->client = $client;
        $this->downloader = $downloader;
    }

    public function testDownloadWith200()
    {
        $response = $this->createResponseMock(200);
        $this->client->shouldReceive('get')->with('test', Mockery::any())->andReturn($response);

        $this->filesystem->shouldReceive('put')->once();

        $attachment = $this->downloader->download('test');

        $this->assertEquals('1000', $attachment->getSize());
        $this->assertEquals('mime', $attachment->getMimeType());
        $this->assertRegExp('/[0-9]{4}\/[A-Za-z]{3}\/[0-9]{2}\/[a-z0-9]{13}\/url/', $attachment->getUrlPath());
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWithNot200()
    {
        $response = $this->createResponseMock(404);
        $this->client->shouldReceive('get')->with('test', Mockery::any())->andReturn($response);

        $this->filesystem->shouldReceive('put')->never();

        $this->downloader->download('test');
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWhenBadResponse()
    {
        $this->createResponseMock(200);
        $badResponseException  = Mockery::mock(\GuzzleHttp\Exception\BadResponseException::class);
        $this->client->shouldReceive('get')->with('test', Mockery::any())->andThrow($badResponseException);

        $this->filesystem->shouldReceive('put')->never();

        $this->downloader->download('test');
    }

    protected function createResponseMock($status)
    {
        $response = Mockery::mock(Response::class);
        $response->shouldReceive('getStatusCode')->andReturn($status);
        $response->shouldReceive('getHeader')->with(RedirectMiddleware::HISTORY_HEADER)->andReturn(['http://effective/url']);
        $response->shouldReceive('getHeader')->with('Content-Type')->andReturn('mime');
        $response->shouldReceive('getHeader')->with('Content-Length')->andReturn(1000);
        $response->shouldReceive('getBody')->andReturn('data');

        return $response;
    }

    public function testValidate()
    {
        $this->assertTrue($this->downloader->validate('http://localhost'));
        $this->assertTrue($this->downloader->validate('https://webiste.com'));
        $this->assertTrue($this->downloader->validate('ftp://webiste.com'));
    }

    public function testValidateNotValid()
    {
        $this->assertFalse($this->downloader->validate(Mockery::mock(Mockery::class)));
        $this->assertFalse($this->downloader->validate(1));
        $this->assertFalse($this->downloader->validate('path/to/file'));
        $this->assertFalse($this->downloader->validate('//website.com'));
    }
}
