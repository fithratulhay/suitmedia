<?php

namespace App\Providers;

use App\Validator\PasswordValidator;
use Illuminate\Support\ServiceProvider;

class SuitcmsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        require app_path('macros.php');
    }

    public function register()
    {
        // pass
    }
}
