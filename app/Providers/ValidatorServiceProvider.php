<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{

    protected $validators = [
        'App\Validator\PasswordValidator'
    ];

    public function boot()
    {
        $this->passwordValidator();
        $this->extensionValidator();
        // This boot validator is deprecated
        // $this->bootValidator();
    }

    public function register()
    {
        // Pass
    }

    protected function bootValidator()
    {
        foreach ($this->validators as $validator) {
            $this->app['validator']->resolver(function ($translator, $data, $rules, $messages) use ($validator) {
                return new $validator($translator, $data, $rules, $messages);
            });
        }
    }

    /**
     * Password related custom validator
     * @return void
     */
    protected function passwordValidator()
    {
        Validator::extend('basic_password', 'App\Validator\PasswordValidator@validateBasicPassword');
        Validator::replacer('basic_password', 'App\Validator\PasswordValidator@replaceBasicPassword');

        Validator::extend('current_password', 'App\Validator\PasswordValidator@validateCurrentPassword');
    }

    protected function extensionValidator()
    {
        /**
         * Validate the dot extension of a file upload is in a set of file extensions.
         * Note : this validator still unstable, please inform error is found.
         */
        Validator::extend('mimext', 'App\Validator\ExtensionValidator@validateExtension');
        Validator::replacer('mimext', 'App\Validator\ExtensionValidator@replaceExtension');
    }
}
