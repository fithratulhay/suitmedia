<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class AdminCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:new-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new super admin.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $username = $this->input->getArgument('username');
        $email = $this->input->getArgument('email');
        $password = $this->input->getArgument('password');

        $adminClass = \Config::get('auth.model');
        $admin = new $adminClass();
        $admin->username = $username;
        $admin->email = $email;
        $admin->password = $password;
        $admin->active = true;
        $admin->group_type = $admin::SUPER_ADMIN;

        if ($admin->save()) {
            $this->info('New Admin has been successfully created!!');
        } else {
            $this->error('Failed to create new admin.');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['username', InputArgument::REQUIRED, 'Admin\' username'],
            ['email', InputArgument::REQUIRED, 'Admin\' email'],
            ['password', InputArgument::REQUIRED, 'Admin\'s password'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
