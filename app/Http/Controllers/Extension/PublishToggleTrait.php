<?php

namespace App\Http\Controllers\Extension;

use Illuminate\Http\Request;

trait PublishToggleTrait
{
    public function publish(Request $request, $key)
    {
        $state = false;
        $model = $this->find($key);

        if (isset($request->state) && $request->state == 'true') {
            $state = true;
        }

        if (!empty($this->model)) {
            $model->published = $state;
            $model->save();
        }
        $response = [
            'message' => 'test',
            'data' => $model
        ];
        return response()->json($response);
    }
}
