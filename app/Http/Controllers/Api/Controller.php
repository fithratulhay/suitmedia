<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use Carbon\Carbon;

abstract class Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
