<?php

\Route::match(
    ['GET', 'POST'],
    suitLoginUrl(),
    [
        'middleware' => 'admin.guest',
        'as' => suitRouteName('login'),
        'uses' => 'SessionController@login'
    ]
);

\Route::controller(suitPath('password'), 'RemindersController');

\Route::group(['prefix' => \Config::get('suitcms.prefix_url'), 'middleware' => 'admin.auth'], function () {
    // ElFinder
    \Route::get(
        'elfinder',
        [
            'as' => suitRouteName('elfinder'),
            'uses' => '\Barryvdh\Elfinder\ElfinderController@showTinyMCE4'
        ]
    );

    \Route::get('/', ['as' => suitRouteName('index'),
                      'uses' => 'SuitcmsController@index']);
    \Route::match(['GET', 'POST'], '/change-password', ['as' => suitRouteName('password'),
                                     'uses' => 'SessionController@changePassword']);
    \Route::get('/logout', ['as' => suitRouteName('logout'),
                                            'uses' => 'SessionController@logout']);

    \Route::resource('pages', 'PageController', ['except' => 'show']);
    \Route::resource('users', 'UserController', ['except' => 'show']);
    \Route::resource('menus', 'MenuController', ['except' => 'show']);
    \Route::resource('settings', 'SettingController', ['except' => 'show']);
    \Route::resource('html-templates', 'HtmlTemplateController', ['except' => 'show']);

    //Template Route
    \Route::get(
        'template/list',
        [
            'as' => suitRouteName('template.list'),
            'uses' => 'HtmlTemplateController@templateList'
        ]
    );
    \Route::get(
        'template/{key}',
        [
            'as' => suitRouteName('template.detail'),
            'uses' => 'HtmlTemplateController@template'
        ]
    );

    // Custom Route to change published in Index
    \Route::post('pages/publish/{key}', [
        'as' => suitRouteName('pages.publish'),
        'uses' => 'PageController@publish'
    ]);
});
