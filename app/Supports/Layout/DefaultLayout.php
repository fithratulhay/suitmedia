<?php

namespace App\Supports\Layout;

use App\Model\Page;
use Illuminate\Http\Request;

class DefaultLayout extends Layout
{
    protected static $name = 'Default Layout';

    public function handle(Request $request, Page $page)
    {
        return view('pages.default', compact('page'));
    }
}
