<?php

namespace App\Supports\Layout;

use App\Model\Page;
use Illuminate\Http\Request;

abstract class Layout
{
    /**
     * Layout Name also use for default view location
     */
    protected static $name = 'Unknown Layout';

    public static function getName()
    {
        return static::$name;
    }

    abstract public function handle(Request $request, Page $page);
}
