<?php

namespace App\Supports\Attachment;

use Illuminate\Support\Facades\Facade;

class ManagerFacade extends Facade
{
    const FILE = 0;
    const YOUTUBE = 1;

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'attachment.manager';
    }
}
