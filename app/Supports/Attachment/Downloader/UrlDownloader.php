<?php

namespace App\Supports\Attachment\Downloader;

use App\Supports\Attachment\File;
use App\Supports\Attachment\ResourceNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RedirectMiddleware;
use Illuminate\Contracts\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesserInterface;

class UrlDownloader extends AbstractDownloader
{

    protected $requestClient;

    protected $requestOptions = [
        'allow_redirects' => [
            'track_redirects' => true
        ]
    ];

    public function __construct(
        Filesystem $filesystem,
        ExtensionGuesserInterface $extensionGuesser,
        Client $requestClient
    ) {
        parent::__construct($filesystem, $extensionGuesser);

        $this->requestClient = $requestClient;
    }

    public function download($url)
    {
        try {
            $response = $this->requestClient->get($url, $this->requestOptions);

            $redirectUrls = $response->getHeader(RedirectMiddleware::HISTORY_HEADER);
            if (!empty($redirectUrls)) {
                $url = $redirectUrls[count($redirectUrls) - 1];
            }

            if ($response->getStatusCode() !== 200) {
                $errorMessage = sprintf("`%s` return response code [%s]", $url, $response->getStatusCode());
                throw new ResourceNotFoundException($errorMessage);
            }

            $urlFilename = basename(explode('?', $url)[0]);
            $filename = $this->filename($urlFilename, $response->getHeader('Content-Type'));
            $urlPath = $this->savePath($filename);

            $this->filesystem->put($urlPath, $response->getBody());

            $result = new File($urlPath, $response->getHeader('Content-Type'), $response->getHeader('Content-Length'));
            return $result;
        } catch (BadResponseException $e) {
            throw new ResourceNotFoundException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function validate($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL) !== false;
    }
}
