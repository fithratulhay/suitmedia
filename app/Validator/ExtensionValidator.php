<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\File;
use Validator;

class ExtensionValidator
{
    /**
     * Validate the dot extension of a file upload is in a set of file extensions.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateExtension($attribute, $value, $parameters)
    {
        $validator = Validator::make([$attribute => $value], [
            $attribute => 'mime:' . implode(',', $parameters),
        ]);

        if (!$validator) {
            return false;
        }

        $parameter = implode('|', $parameters);

        if ($value instanceof File) {
            $value = $value->getClientOriginalName();
        }

        return preg_match("/^.*\.($parameter)$/", $value);
    }

    /**
     * Replace all place-holders for the file extension rule.
     *
     * @param  string  $message
     * @return string
     */
    public function replaceExtension($message, $attribute, $rule, $parameters)
    {
        $parameter = implode(', ', $parameters);
        return str_replace(':values', $parameter, $message);
    }
}
