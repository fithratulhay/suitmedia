<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHtmlTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('html_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->index()->nullable();
            $table->string('title');
            $table->string('description')->nullable();
            $table->text('html');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('html_templates');
    }
}
