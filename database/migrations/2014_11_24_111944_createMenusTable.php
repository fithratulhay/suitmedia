<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('menus', function ($table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->boolean('is_link')->default(false);
            $table->string('type')->index()->default('Main');
            $table->string('url')->default('');
            $table->integer('order')->default(0);
            $table->timestamps();

            $table->foreign('parent_id')
                  ->references('id')->on('menus')
                  ->onDelete('cascade');
        });

        \Schema::create('menus_translate', function (Blueprint $table) {
            $table->integer('menu_id')->unsigned();
            $table->string('lang', 10)->index();
            $table->string('title');

            $table->foreign('menu_id')
                  ->references('id')->on('menus')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('menus_translate');
        \Schema::dropIfExists('menus');
    }
}
